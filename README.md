# GitLab Alfred Workflow

A small collection of useful Alfred workflows with live results.

![Alfred example](workflow.png)

## Commands

### GitLab Handbook
`glh SEARCHTERM`

Search in GitLab Handbook

### GitLab Docs
`gld SEARCHTERM`

Search in GitLab Docs

### GitLab Releases
`glr`

Show latest GitLab Releases

### GitLab (All) Releases
`gla`

Show All GitLab Releases

### GitLab Blog
`glb`

Show latest from GitLab Blog

### GitLab Pajamas Design System
`glp`

Show components etc. in the Design System documentation.

#### Update content
`glu`

Show components etc. in the Design System documentation.
